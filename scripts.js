const passwordInput = document.getElementById("password-input");
const confirmPasswordInput = document.getElementById("confirm-password-input");
const passwordToggle = document.getElementById("password-toggle");
const confirmToggle = document.getElementById("confirm-password-toggle");

function togglePasswordVisibility(inputElement, toggleElement) {
  if (inputElement.type === "password") {
    inputElement.type = "text";
    toggleElement.classList.remove("fa-eye");
    toggleElement.classList.add("fa-eye-slash");
  } else {
    inputElement.type = "password";
    toggleElement.classList.remove("fa-eye-slash");
    toggleElement.classList.add("fa-eye");
  }
}

passwordToggle.addEventListener("click", () => {
  togglePasswordVisibility(passwordInput, passwordToggle);
});

confirmToggle.addEventListener("click", () => {
  togglePasswordVisibility(confirmPasswordInput, confirmToggle);
});

document.querySelector(".password-form").addEventListener("submit", (e) => {
  e.preventDefault();

  const passwordValue = passwordInput.value;
  const confirmPasswordValue = confirmPasswordInput.value;

  if (passwordValue === confirmPasswordValue) {
    alert("You are welcome");
  } else {
    const errorMessage = document.createElement("p");
    errorMessage.textContent = "Потрібно ввести однакові значення";
    errorMessage.style.color = "red";
    const form = document.querySelector(".password-form");
    form.insertBefore(errorMessage, form.lastChild);
  }
});
